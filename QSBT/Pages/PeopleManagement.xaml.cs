﻿using QSBT.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QSBT.Pages
{
    /// <summary>
    /// Interaction logic for PeopleManagement.xaml
    /// </summary>
    public partial class PeopleManagement : Page
    {
        private List<User> userList;
        //private readonly object a;

        public PeopleManagement()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            Switcher.mainWindow.btn_Logout.Visibility = Visibility.Visible;
            Switcher.mainWindow.btn_Back.Visibility = Visibility.Visible;
            FakeData();
        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            Switcher.mainWindow.btn_Logout.Visibility = Visibility.Hidden;
            Switcher.mainWindow.btn_Back.Visibility = Visibility.Hidden;
        }

        private void tbCMND_TextChanged(object sender, TextChangedEventArgs e)
        {
            var result = userList.Where(u => u.Username.Contains(tbCMND.Text)).ToList();
            GridPeople.ItemsSource = result;
        }

        private void tbHoTen_TextChanged(object sender, TextChangedEventArgs e)
        {
            var result = userList.Where(u => u.Password.Contains(tbHoTen.Text)).ToList();
            GridPeople.ItemsSource = result;
        }

        //TODO: using combobox to search year
        private void FakeData()
        {
            var abc = new List<User>();
            var user = new User();
            for (int i = 0; i < 5; i++)
            {

                abc.Add(new User { Username = "abc" + i, Password = "abd" + (i + 1), UserId = i });
            }
            userList = abc;
            GridPeople.ItemsSource = userList;
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
