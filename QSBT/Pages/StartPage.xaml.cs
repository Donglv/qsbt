﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QSBT.Pages
{
    /// <summary>
    /// Interaction logic for StartPage.xaml
    /// </summary>
    public partial class StartPage : Page
    {
        public StartPage()
        {
            InitializeComponent();
        }

        private void btnPeople_Click(object sender, RoutedEventArgs e)
        {
            Switcher.ChangePage(Switcher.Pages.PeopleManagement);
        }

        #region Start Page Event
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            Switcher.mainWindow.btn_Back.Visibility = Visibility.Hidden;
            Switcher.mainWindow.btn_Logout.Visibility = Visibility.Visible;
        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            Switcher.mainWindow.btn_Back.Visibility = Visibility.Hidden;
            Switcher.mainWindow.btn_Logout.Visibility = Visibility.Hidden;
        }
        #endregion
    }
}
