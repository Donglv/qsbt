﻿using QSBT.Services.Implementations;
using QSBT.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using QSBT.Session;
using QSBT.Model;

namespace QSBT.Pages
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class LoginPage : Page
    {
        private static readonly IUserService _userService = new UserService();
        private static List<User> _userList = new List<User>();

        public LoginPage()
        {
            ThreadPool.QueueUserWorkItem(delegate { GetAllUsers(); });
            InitializeComponent();
        }

        private static void GetAllUsers()
        {
            _userList = _userService.GetAllUser();
        }

        private bool CheckField()
        {
            if (tb_UserName.Text == "")
            {
                Switcher.mainWindow.SetPopupMessage("Vui Lòng Nhập Tên Đăng Nhập");
                tb_UserName.Focus();
                return false;
            }
            else if (tb_Password.Password == "")
            {
                Switcher.mainWindow.SetPopupMessage("Vui Lòng Nhập Mật Khẩu");
                tb_Password.Focus();
                return false;
            }
            return true;
        }

        #region Login Page Event
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            tb_UserName.Focus();
            ClearInputData();
        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            ClearInputData();
        }
        #endregion

        #region Button Event
        private void btn_Login_Click(object sender, RoutedEventArgs e)
        {
            //TODO: Check user role and permission
            var isInputValid = CheckField();

            if (isInputValid)
            {
                var username = tb_UserName.Text;
                var password = tb_Password.Password;

                var _user = _userList.Where(u => u.Username == username).SingleOrDefault();

                if (_user != null)
                {
                    if (_user.Password == password)
                    {
                        Dispatcher.Invoke(delegate
                        {
                            Switcher.ChangePage(Switcher.Pages.StartPage);
                        });

                        ClearInputData();

                        //Set user Session here
                        ThreadPool.QueueUserWorkItem(delegate
                        {
                            UserSession.Instance.Username = _user.Username;
                            UserSession.Instance.Password = _user.Password;
                            UserSession.Instance.UserId = _user.UserId;
                        });
                    }
                    else
                    {
                        Switcher.mainWindow.SetPopupMessage("Sai Mật Khẩu");
                        tb_Password.Clear();
                        tb_Password.Focus();
                    }
                }
                else
                {
                    Switcher.mainWindow.SetPopupMessage("Sai Tên Đăng Nhập");
                    tb_UserName.Clear();
                    tb_UserName.Focus();
                }
            }
        }

        private void btn_Clear_Click(object sender, RoutedEventArgs e)
        {
            ClearInputData();
        }

        private void btn_Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
        #endregion

        #region Makeup Page
        private void ClearInputData()
        {
            tb_UserName.Clear();
            tb_Password.Clear();
        }

        private void SetTiltle()
        {

        }
        #endregion
    }
}
