﻿using QSBT.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace QSBT
{
    public static class Switcher
    {
        public static MainWindow mainWindow;

        private static StartPage StartPage = null;
        private static LoginPage LoginPage = null;
        private static PeopleManagement PeopleManagement = null;
        //private static DocumentManagement DocumentManagement = null;

        public static void MainWindowNavigate(Page newPage)
        {
            mainWindow.MainContainer.Navigate(newPage);
        }

        public static void ChangePage(Pages page)
        {
            try
            {
                switch (page)
                {
                    case Pages.LoginPage:
                        if (LoginPage == null) { LoginPage = new LoginPage(); }
                        MainWindowNavigate(LoginPage);
                        break;
                    case Pages.StartPage:
                        if (StartPage == null) { StartPage = new StartPage(); }
                        MainWindowNavigate(StartPage);
                        break;
                    case Pages.PeopleManagement:
                        if (PeopleManagement == null) { PeopleManagement = new PeopleManagement(); }
                        MainWindowNavigate(PeopleManagement);
                        break;
                }
                GC.Collect();
            }
            catch { }
        }

        public static void BackPage(Pages page)
        {
            try
            {
                switch (page)
                {
                    case Pages.StartPage:
                        if (StartPage == null) { LoginPage = new LoginPage(); }
                        break;
                }
                GC.Collect();
            } 
            catch { }
        }

        public enum Pages
        {
            StartPage = 1,
            LoginPage = 0,
            PeopleManagement = 2,
            DocumentManagement = 3
        }
    }
}
