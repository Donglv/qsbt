﻿using QSBT.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace QSBT
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool _allowDirectNavigation = false;
        private NavigatingCancelEventArgs _navArgs = null;
        private Duration _duration = new Duration(TimeSpan.FromSeconds(0.3));

        public MainWindow()
        {
            InitializeComponent();
            Switcher.mainWindow = this;
            this.MainContainer.NavigationUIVisibility = System.Windows.Navigation.NavigationUIVisibility.Hidden;

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();

            SetAppVersion();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            lblTimer.Content = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
        }

        private void SetAppVersion()
        {
            var appInfo = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            lblVersion.Content = "v." + appInfo;
        }

        private void ClearUserSession()
        {
            ThreadPool.QueueUserWorkItem(delegate {
                UserSession.Instance.Username = null;
                UserSession.Instance.Password = null;
                UserSession.Instance.UserId = 0;
            });
        }

        public void SetPopupMessage(string message)
        {
            lblMessage.Content = message;
            PopupMessage.Visibility = Visibility.Visible;
        }

        #region Window Slide Animation
        private void MainContainer_Navigating(object sender, NavigatingCancelEventArgs e)
        {
            if (Content != null && !_allowDirectNavigation)
            {
                e.Cancel = true;

                _navArgs = e;

                DoubleAnimation transAnimation = new DoubleAnimation();
                transAnimation.From = 1;
                transAnimation.To = 0;
                transAnimation.Duration = _duration;
                transAnimation.Completed += SlideCompleted;
                this.MainContainer.BeginAnimation(OpacityProperty, transAnimation);
            }
            _allowDirectNavigation = false;
        }

        private void SlideCompleted(object sender, EventArgs e)
        {
            _allowDirectNavigation = true;
            switch (_navArgs.NavigationMode)
            {
                case NavigationMode.New:
                    if (_navArgs.Uri == null)
                        this.MainContainer.Navigate(_navArgs.Content);
                    else
                        this.MainContainer.Navigate(_navArgs.Uri);
                    break;
                case NavigationMode.Back:
                    this.MainContainer.GoBack();
                    break;
                case NavigationMode.Forward:
                    this.MainContainer.GoForward();
                    break;
                case NavigationMode.Refresh:
                    this.MainContainer.Refresh();
                    break;
            }

            Dispatcher.BeginInvoke(DispatcherPriority.Loaded,
                (ThreadStart)delegate ()
                {
                    DoubleAnimation transAnimation = new DoubleAnimation();
                    transAnimation.From = 0;
                    transAnimation.To = 1;
                    transAnimation.Duration = _duration;
                    this.MainContainer.BeginAnimation(OpacityProperty, transAnimation);
                });
        }
        #endregion

        #region Main Window Event
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            btn_Back.Visibility = Visibility.Hidden;
            btn_Logout.Visibility = Visibility.Hidden;
        }

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {

        }
        #endregion

        #region Main Window Button Event
        private void btn_Logout_Click(object sender, RoutedEventArgs e)
        {
            //TODO: Clear user session here
            ClearUserSession();
            Switcher.ChangePage(Switcher.Pages.LoginPage);
        }

        private void btn_Back_Click(object sender, RoutedEventArgs e)
        {
            //Switcher.ChangePage(Switcher.Pages.LoginPage);
            Switcher.ChangePage(Switcher.Pages.StartPage);
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            PopupMessage.Visibility = Visibility.Collapsed;
        }
        #endregion
    }
}
