﻿using QSBT.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QSBT.Session
{
    [NotMapped]
    public class UserSession: User
    {
        private static UserSession _instance;

        private UserSession() { }

        public static UserSession Instance
        {
            get
            {
                return _instance ?? (_instance = new UserSession());
            }
        }
    }
}
