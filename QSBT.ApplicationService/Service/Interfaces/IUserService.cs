﻿using QSBT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QSBT.Services.Interfaces
{
    public interface IUserService
    {
        List<User> GetAllUser();
        User GetUserByUsername(string userName);
        bool CreateUser(User user);
    }
}
