﻿using QSBT.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QSBT.Model;
using QSBT.DataContext;
using QSBT.ApplicationService;

namespace QSBT.Services.Implementations
{
    public class UserService : IUserService
    {
        public List<User> GetAllUser()
        {
            try
            {
                var context = ServiceClient.GetDataContext();

                var _users = context.User.Select(u => u).ToList();
                context.Dispose();
                GC.Collect();

                return _users;
            }
            catch (Exception ex)
            {
                GC.Collect();
                throw new Exception(ex.Message);
            }

        }

        public User GetUserByUsername(string userName)
        {
            try
            {
                var context = ServiceClient.GetDataContext();

                var _user = context.User.Where(u => u.Username == userName)
                                        .SingleOrDefault();
                context.Dispose();
                GC.Collect();

                return _user;
            }
            catch (Exception ex)
            {
                GC.Collect();
                throw new Exception(ex.Message);
            }
        }

        public bool CreateUser(User user)
        {
            try
            {
                var context = ServiceClient.GetDataContext();

                var _user = context.User.Add(user);
                context.SaveChanges();
                context.Dispose();
                GC.Collect();

                return true;
            }
            catch (Exception ex)
            {
                GC.Collect();
                throw new Exception(ex.Message);
            }
        }
    }
}
