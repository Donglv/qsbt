﻿using QSBT.DataContext;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QSBT.ApplicationService
{
    public static class ServiceClient
    {
        public static QSBTDataContext GetDataContext()
        {
            return new QSBTDataContext();
        }
    }
}
